import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from './themes/material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { ComponentsComponent } from './components/components.component';
import { TransactionItemComponent } from './transactions/transaction-item/transaction-item.component';
import { HttpClientModule } from '@angular/common/http';
import { ItemComponent } from './components/transactions/item/item.component';
import { AddItemComponent } from './components/transactions/add-item/add-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TransactionsComponent,
    ComponentsComponent,
    TransactionItemComponent,
    ItemComponent,
    AddItemComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
