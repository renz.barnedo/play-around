import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Transaction } from 'src/app/models/transaction';
import { TransactionService } from 'src/app/services/transaction.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css'],
})
export class AddItemComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private transactionService: TransactionService,
    private uiService: UiService,
    @Inject(MAT_DIALOG_DATA) private transaction: Transaction
  ) {}

  ngOnInit(): void {
    this.setFormControls();
  }

  transactionForm: FormGroup = this.fb.group({});
  setFormControls(): void {
    const transaction = this.transaction;
    this.transactionForm = this.fb.group({
      id: this.fb.control(transaction?.id),
      category: this.fb.control(transaction?.category, Validators.required),
      description: this.fb.control(
        transaction?.description,
        Validators.required
      ),
      quantity: this.fb.control(transaction?.quantity, Validators.required),
      price: this.fb.control(transaction?.price, Validators.required),
      status: this.fb.control(transaction?.status, Validators.required),
      date: this.fb.control(transaction?.date, Validators.required),
    });
  }

  submitTransactionForm() {
    if (this.transactionForm.status !== 'VALID') {
      // notify user for errors
      return;
    }
    if (this.transactionForm.value.id) {
      this.updateTransaction();
      return;
    }
    this.addTransaction();
  }

  async addTransaction() {
    try {
      await this.transactionService.add(this.transactionForm.value);
      this.uiService.closeDialog();
    } catch (error) {
      console.log(error);
    }
  }

  async updateTransaction() {
    try {
      await this.transactionService.update(this.transactionForm.value);
      this.uiService.closeDialog();
    } catch (error) {
      console.log(error);
    }
  }
}
