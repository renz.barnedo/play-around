import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent implements OnInit {
  @Input() transaction: any;
  @Output() onUpdate = new EventEmitter();
  @Output() onDelete = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  update(): void {
    this.onUpdate.emit(this.transaction);
  }

  delete() {
    this.onDelete.emit(this.transaction);
  }
}
