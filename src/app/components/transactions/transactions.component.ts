import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Transaction } from 'src/app/models/transaction';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css'],
})
export class TransactionsComponent implements OnInit {
  @Input() transactions: Transaction[] = [];
  @Output() onUpdate = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  showItem = false;

  constructor() {}

  ngOnInit(): void {
    this.showItem = true;
  }

  update(transaction: Transaction) {
    this.onUpdate.emit(transaction);
  }

  delete(transaction: Transaction) {
    this.onDelete.emit(transaction);
  }
}
