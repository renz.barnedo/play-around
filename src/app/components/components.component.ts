import { Component, OnInit } from '@angular/core';
import { Transaction } from '../models/transaction';
import { TransactionService } from '../services/transaction.service';
import { UiService } from '../services/ui.service';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css'],
})
export class ComponentsComponent implements OnInit {
  constructor(
    private transactionService: TransactionService,
    private uiService: UiService
  ) {}

  ngOnInit(): void {
    this.getTransactions();
  }

  transactions: Transaction[] = [];
  showTransactions = false;

  async getTransactions() {
    try {
      this.transactions = await this.transactionService.getTransactions();
      this.showTransactions = true;
    } catch (error) {
      console.log(error);
    }
  }

  openAddDialog() {
    const addDialog = this.uiService.openAddDialogForm();
    addDialog.afterClosed().subscribe((result) => this.getTransactions());
  }

  update(transaction: Transaction) {
    const updateDialog = this.uiService.openUpdateDialogForm(transaction);
    updateDialog.afterClosed().subscribe((result) => this.getTransactions());
  }

  async delete(id: number) {
    try {
      await this.transactionService.delete(id);
      await this.getTransactions();
    } catch (error) {
      console.log(error);
    }
  }
}
