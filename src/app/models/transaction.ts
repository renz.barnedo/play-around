export interface Transaction {
  id?: number;
  category: string;
  description: string;
  quantity: number;
  price: number;
  status: string;
  date: string;
}
