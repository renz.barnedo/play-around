import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddItemComponent } from '../components/transactions/add-item/add-item.component';
import { Transaction } from '../models/transaction';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  constructor(private dialog: MatDialog) {}

  openAddDialogForm() {
    const addDialog = this.dialog.open(AddItemComponent, {
      disableClose: true,
    });
    return addDialog;
  }

  closeDialog() {
    this.dialog.closeAll();
  }

  openUpdateDialogForm(transaction: Transaction) {
    const addDialog = this.dialog.open(AddItemComponent, {
      disableClose: true,
      data: transaction,
    });
    return addDialog;
  }
}
