import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Transaction } from '../models/transaction';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  private ROOT_API_URL = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getTransactions() {
    const apiUrl = this.ROOT_API_URL + '/transactions';
    const promise: any = new Promise((resolve, reject) =>
      this.http.get<Transaction>(apiUrl).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      )
    );
    return promise;
  }

  add(transaction: Transaction) {
    const apiUrl = this.ROOT_API_URL + '/transactions';
    const promise: any = new Promise((resolve, reject) =>
      this.http.post(apiUrl, transaction).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      )
    );
    return promise;
  }

  update(transaction: Transaction) {
    const apiUrl = this.ROOT_API_URL + `/transactions/${transaction.id}`;
    const promise = new Promise((resolve, reject) =>
      this.http.patch<Transaction>(apiUrl, transaction).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      )
    );
    return promise;
  }

  delete(id: number) {
    const apiUrl = this.ROOT_API_URL + `/transactions/${id}`;
    const promise = new Promise((resolve, reject) =>
      this.http.delete<Transaction>(apiUrl).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      )
    );
    return promise;
  }
}
